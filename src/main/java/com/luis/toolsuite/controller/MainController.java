package com.luis.toolsuite.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {

	@RequestMapping("/")
	public ModelAndView index() {
		return new ModelAndView("downsample");
	}

	@RequestMapping("/downsample")
	public ModelAndView downsample() {
		return new ModelAndView("downsample");
	}

	@RequestMapping("/kmeans")
	public ModelAndView kmeans() {
		return new ModelAndView("kmeans");
	}

	@RequestMapping("/isforest")
	public ModelAndView isforest() {
		return new ModelAndView("isforest");
	}
}
