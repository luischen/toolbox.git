package com.luis.toolsuite.kmeans.model;

import com.luis.toolsuite.util.KMeansUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Cluster {
    private Double[] center; // 质心
    private List<Double[]> pointList; // 属于当前簇的所有点
    private BigDecimal sse; // 簇内平方和

    public Cluster(Double[] c){
        this.center = c;
        pointList = new ArrayList<>();
        sse = BigDecimal.ZERO;
    }

    public Double[] getCenter() {
        return center;
    }

    public List<Double[]> getPointList() {
        return pointList;
    }

    public void addPoint(Double[] point) {
        this.pointList.add(point);
    }

    public BigDecimal getSse() {
        return sse;
    }

    //计算簇内平方和
    public void calculateSSE(){
        BigDecimal tempSSE = BigDecimal.ZERO;
        for(Double[] point : pointList){
            tempSSE = tempSSE.add(KMeansUtil.calculateDistance(point, center));
        }
        this.sse = tempSSE;
    }

    public void print(){
        System.out.println("质心["+ Arrays.toString(center));
        pointList.stream().map(Arrays::toString).forEach(System.out::println);
        System.out.println("cluster end.");
    }

}
