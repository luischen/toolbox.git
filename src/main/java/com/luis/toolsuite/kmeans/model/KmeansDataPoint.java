package com.luis.toolsuite.kmeans.model;

import javax.persistence.*;

@Entity
public class KmeansDataPoint {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	Double value;
	Double value2;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public Double getValue2() {
		return value2;
	}
	public void setValue2(Double value2) {
		this.value2 = value2;
	}
	
}
