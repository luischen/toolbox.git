package com.luis.toolsuite.service;

import com.luis.toolsuite.isolationforest.model.IfDataPoint;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IfDataRepository extends CrudRepository<IfDataPoint, Integer>{

	@Query("select t from IfDataPoint t where t.time < '2000-01-01'")
	List<IfDataPoint> findForTrain();

	@Query("select t from IfDataPoint t where t.time > '2020-01-01'")
	List<IfDataPoint> findForTest();
}
