package com.luis.toolsuite.service;

import com.luis.toolsuite.kmeans.model.KmeansDataPoint;
import org.springframework.data.repository.CrudRepository;

public interface KmeansDataRepository extends CrudRepository<KmeansDataPoint, Integer>{

}
