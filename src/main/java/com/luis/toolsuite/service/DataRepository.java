package com.luis.toolsuite.service;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.luis.toolsuite.downsampling.model.DataPoint;

public interface DataRepository extends CrudRepository<DataPoint, Integer>{

	@Query("select t from DataPoint t where t.relId =  ?1")
	List<DataPoint> findByRelId(Integer relId,Pageable pageable);
}
