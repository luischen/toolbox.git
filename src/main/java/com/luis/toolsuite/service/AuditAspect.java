package com.luis.toolsuite.service;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AuditAspect {

	@Pointcut("execution(* com.luis.toolsuite.downsampling.service.DownsampleStrategy.downsample(..))")
	public void downsamplePoint() {}
	
	@Around("downsamplePoint()")
	public Object recordRuntime(ProceedingJoinPoint jp) {
		long start = System.currentTimeMillis();
		Object obj = null;
		try {
			obj = jp.proceed(jp.getArgs());
		} catch (Throwable e) {
			e.printStackTrace();
		}
		long end = System.currentTimeMillis();
		System.out.println("total used for "+ jp.getTarget().getClass().getSimpleName() + " " +(end- start));
		return obj;
	}
}
