package com.luis.toolsuite.downsampling.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class DataPoint {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	Integer relId;
	Long time;
	Double value;
	Double value2;
	
	@Transient
	Double score;
	@Transient
	BigDecimal area;//用于LT算法的面积
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getRelId() {
		return relId;
	}
	public Long getTime() {
		return time;
	}
	public void setRelId(Integer relId) {
		this.relId = relId;
	}
	public void setTime(Long time) {
		this.time = time;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public Double getValue2() {
		return value2;
	}
	public void setValue2(Double value2) {
		this.value2 = value2;
	}
	public Double getScore() {
		return score;
	}
	public void setScore(Double score) {
		this.score = score;
	}
	public BigDecimal getArea() {
		return area;
	}
	public void setArea(BigDecimal area) {
		this.area = area;
	}
	
}
