package com.luis.toolsuite.downsampling.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.luis.toolsuite.downsampling.model.DataPoint;
import com.luis.toolsuite.downsampling.service.DownsampleStrategy;
import com.luis.toolsuite.util.Helper;

@Service("LTOB")
public class LTOBStrategy implements DownsampleStrategy{

	@Override
	public List<DataPoint> downsample(List<DataPoint> original) {
		// step 1计算每个点的面积
		if(original.size() < 3) return original;
		for(int i = 2; i< original.size();i++) {
			//每次计算中间点的面积
			BigDecimal area = Helper.calcTriangleArea(original.get(i-2), original.get(i-1), original.get(i));
			original.get(i-1).setArea(area);
		}
		//step 2分桶
		List<List<DataPoint>> bucketList = Helper.split(original);
		//step 3获取每个桶的最大面积
		return bucketList.stream().map(bucket->{
			if(bucket.size() == 1) return bucket.get(0);
			//获取面积最大的点
			return bucket.stream().max((p1,p2)-> p1.getArea().compareTo(p2.getArea())).get();
		}).collect(Collectors.toList());
	}

}
