package com.luis.toolsuite.downsampling.service;

import java.util.List;

import com.luis.toolsuite.downsampling.model.DataPoint;

public interface DownsampleStrategy {

	List<DataPoint> downsample(List<DataPoint> original);
	
}
