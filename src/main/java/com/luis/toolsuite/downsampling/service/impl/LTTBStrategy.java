package com.luis.toolsuite.downsampling.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.luis.toolsuite.downsampling.model.DataPoint;
import com.luis.toolsuite.downsampling.service.DownsampleStrategy;
import com.luis.toolsuite.util.Helper;

/**largest triangle three bucket*/
@Service("LTTB")
public class LTTBStrategy implements DownsampleStrategy{

	@Override
	public List<DataPoint> downsample(List<DataPoint> original) {
		//step 1 split into bucket
		List<List<DataPoint>> bucketList = Helper.split(original);
		//至少要有3个bucket才能适用这个算法
		if(bucketList.size() < 3) return original;
		List<DataPoint> result = new ArrayList<>();
		result.add(original.get(0));
		DataPoint firstPoint = original.get(0);
		for(int i = 2 ;i < bucketList.size();i++) {
			List<DataPoint> midBucket = bucketList.get(i-1);
			DataPoint lastPoint = Helper.getMedianPoint(bucketList.get(i));//后一个点是后一个bucket的中位点
			DataPoint midPoint = Helper.getLargetTriangelPoint(firstPoint,lastPoint,midBucket);
			result.add(midPoint);
			firstPoint = midPoint;
		}
		result.add(original.get(original.size()-1));
		return result;
	}

}
