package com.luis.toolsuite.downsampling.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.luis.toolsuite.downsampling.model.DataPoint;
import com.luis.toolsuite.downsampling.service.DownsampleStrategy;
import com.luis.toolsuite.util.Helper;

/**mode median bucket strategy*/
@Service("MMB")
public class MMBStrategy implements DownsampleStrategy{

	@Override
	public List<DataPoint> downsample(List<DataPoint> original) {
		//step 1 split into bucket
		List<List<DataPoint>> bucketList = Helper.split(original);
		return bucketList.stream().map(bucket->{
			if(bucket.size() == 1) return bucket.get(0);
			//众数模式
			DataPoint modePoint = Helper.getModePoint(bucket);
			if(modePoint != null) return modePoint;
			//中位数
			DataPoint medianPoint = Helper.getMedianPoint(bucket);
			if(medianPoint != null) return medianPoint;
			return null;
		}).collect(Collectors.toList());
	}
	
	

}
