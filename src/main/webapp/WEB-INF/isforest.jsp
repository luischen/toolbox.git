<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery/jquery_2.2.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/js/echarts/echarts_4.1.0.rc2.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/js/other/moment.js_2.18.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/js/other/moment_2.18.1_locale_zh-cn.js"></script>

    <title>孤立森林股票数据分析</title>
    <style>
        table tr td{
            width:150px;
            padding:5px;
            border: 1px solid;
        }
    </style>
</head>
<body>

<div id="myChart" style="width:1200px;height:800px;margin:40px 20px;float:left;"></div>
<div style="width:400px;margin:100px 40px;text-align: right;font-size:20px;float:left;">
    <table>
        <tr>
            <td>训练数据点</td>
            <td><div id="trainSize"></div></td>
        </tr>
        <tr>
            <td>训练耗时</td>
            <td><div id="trainCost"></div></td>
        </tr>
        <tr>
            <td>测试数据点</td>
            <td><div id="testSize"></div></td>
        </tr>
        <tr>
            <td>测试耗时</td>
            <td><div id="testCost"></div></td>
        </tr>
    </table>、
</div>
</body>
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('myChart'),null,{width:1200,height:800});
    $(function(){
        var scr_url = "/isforest/data";
        drawChart(scr_url);
    })

    function drawChart(request_url){
        var COLOR_ALL = [
            '#37A2DA',
            '#e06343',
            '#37a354',
            '#b55dba',
            '#b5bd48',
            '#8378EA',
            '#96BFFF'
        ];
        var option = {
            tooltip: {
                trigger: 'item'
            },
            xAxis: {
                scale: true,
                type:'time',
                axisLabel:{
                    formatter: function (params) {
                        return moment(new Date(parseInt(params))).format('YYYY-MM-DD');
                    }
                }
            },
            yAxis: {
                scale: true,
                axisLabel: {
                    formatter: function (value, index) {
                        return value.toFixed(2);
                    }
                }
            },
            series: [],
            grid:{left:150},
            dataZoom:[{
                type: 'slider',
                realtime: false,
                show: true,
                xAxisIndex: [0],
                start: 0,
                textStyle:{
                    fontSize:11
                }
            },{
                type: 'slider',
                realtime: false,
                show: true,
                yAxisIndex: [0],
                start: 0,
                textStyle:{
                    fontSize:11
                }
            }],
            color:COLOR_ALL
        };
        myChart.setOption(option);
        myChart.showLoading();
        $.get(request_url, function(result){
            $("#trainSize").html(result.train_size);
            $("#trainCost").html(result.train_cost);
            $("#testSize").html(result.calc_size);
            $("#testCost").html(result.calc_cost);
            option.series.push({
                name: "孤立森林回测",
                type: 'scatter',
                symbolSize: 5,
                showAllSymbol: true,
                data: result.data,
                smooth: false,
                animation: false,
                itemStyle:{
                    color:function(params){
                        return params.data[2] >0 ?'red' : 'green'
                    }
                },
                tooltip:{
                    formatter:function(params){
                        var data0 = params.data;
                        return "涨跌幅："+ data0[2] +"% <br>" +
                            "昨收："+ data0[3] +" <br>" +
                            "开盘价："+ data0[4] +" <br>" +
                            "收盘价："+ data0[5] +" <br>" +
                            "最高价："+ data0[6] +" <br>" +
                            "最低价："+ data0[7] ;
                    }
                }

            });
            myChart.setOption(option);
            myChart.hideLoading();


        });
    }

</script>
</html>