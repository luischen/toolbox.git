<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery/jquery_2.2.0.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/echarts/echarts_4.1.0.rc2.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/other/moment.js_2.18.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/other/moment_2.18.1_locale_zh-cn.js"></script>
<title>Downsampling Compare</title>
<style>
  button{
    width:160px;
    height:40px;    
    margin-left:15px;
    margin-right:15px;
    float:left;
    outline:0px !important;
  }
   button:hover,button:focus,button:active{
	background-color: #576298;
	color: white;
	border-color: #576298;
}
</style>
</head>
<body>
<div id="myChart" style="width:90%;height:200px;float:left"></div>
<div id="mmbChart" style="width:90%;height:200px;float:left"></div>
<div id="ltobChart" style="width:90%;height:200px;float:left"></div>
<div id="lttbChart" style="width:90%;height:200px;float:left"></div>
<div id="ltdChart" style="width:90%;height:200px;float:left"></div>
</body>
<script type="text/javascript">
var myChart = echarts.init(document.getElementById('myChart'));
var chart1 = echarts.init(document.getElementById('mmbChart'));
var chart2 = echarts.init(document.getElementById('ltobChart'));
var chart3 = echarts.init(document.getElementById('lttbChart'));
var chart4 = echarts.init(document.getElementById('ltdChart'));
$(function(){
	initChart();
	scr_url="/data";
	drawChart(scr_url);
}) 

function initChart(){
	var option = {
		    xAxis: {
		    	name: "",
	            scale: true,
	            type:'time',
	            axisLabel:{
	                formatter: function (params) {
	                    return moment(new Date(parseInt(params))).format('YYYY-MM-DD');
	                }
	            }
		    },
		    yAxis: {
	            name: "",
	            scale: true,
	            axisLabel: {                   
	                formatter: function (value, index) {           
	                   return value.toFixed(7);      
	                }                
	            }
	        },
		    series: [],
		    dataZoom:[{
	            type: 'slider',
	            realtime: false,
	            show: true,
	            xAxisIndex: [0],
	            start: 0,
	            textStyle:{
	            	fontSize:11
	            }
	        }]
	};
	myChart.setOption(option);
	chart1.setOption(option);
	chart2.setOption(option);
	chart3.setOption(option);
	chart4.setOption(option);
}

function drawChart(request_url){
	myChart.showLoading();
	chart1.showLoading();
	chart2.showLoading();
	chart3.showLoading();
	chart4.showLoading();
	$.get(request_url, function(result){
		var legendData = [];
		var option = {
				  series:[]	  
		};
		$.each(result, function(key, value){
	    	  legendData.push(key);
	    	  option.series.push({
	        	 name: key,
	   	         type: 'line',
	   	         symbolSize: 1,
	   	         showAllSymbol: true,
	   	         data: value,
	   	      	 smooth: false,
	   	     	 animation: false,
	     	  });
		  });
		option.legend = {
			  data:legendData,
		      top: 40,
		      right: 30
		};
		myChart.setOption(option);
		myChart.hideLoading();
		drawSubChart(chart1,["MMB数据","原始数据"],result["MMB"],result["原始数据"]);
		drawSubChart(chart2,["LTOB数据","原始数据"],result["LTOB"],result["原始数据"]);
		drawSubChart(chart3,["LTTB数据","原始数据"],result["LTTB"],result["原始数据"]);
		drawSubChart(chart4,["LTD数据","原始数据"],result["LTD"],result["原始数据"]);
    });
}

function drawSubChart(chart0, titleArray, data, data_full){
	console.log(data.length);
	var option = {		    
		    series: [{
	        	 name: titleArray[0],
	   	         type: 'line',
	   	         symbolSize: 1,
	   	         showAllSymbol: true,
	   	         data: data,
	   	      	 smooth: false,
	   	     	 animation: false,
	     	  },{
		        	 name: titleArray[1],
		   	         type: 'line',
		   	         symbolSize: 1,
		   	         showAllSymbol: true,
		   	         data: data_full,
		   	      	 smooth: false,
		   	     	 animation: false,
		     	  }]
	};
	option.legend = {
			titleArray,
		      top: 40,
		      right: 30
	};
	chart0.setOption(option);
	chart0.hideLoading();
}
</script>
</html>