<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/jquery/jquery_2.2.0.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/echarts/echarts_4.1.0.rc2.min.js"></script>

<title>KMeans</title>
<style>
  table tr td{
      width:150px;
      padding:5px;
      border: 1px solid;
  }
</style>
</head>
<body>

<div id="myChart" style="width:1000px;height:800px;margin:40px 20px;float:left;"></div>
<div style="width:400px;margin:100px 40px;text-align: right;font-size:20px;float:left;">
    <table>
        <tr>
            <td>总数据点</td>
            <td><div id="totalNumber"></div></td>
        </tr>
        <tr>
            <td>分类数</td>
            <td><div id="classNumber"></div></td>
        </tr>
        <tr>
            <td>总计循环次数</td>
            <td><div id="iteration"></div></td>
        </tr>
        <tr>
            <td>簇内平方和</td>
            <td><div id="sse"></div></td>
        </tr>
    </table>、
</div>
</body>
<script type="text/javascript">
var myChart = echarts.init(document.getElementById('myChart'),null,{width:1000,height:800});
$(function(){
    var classNum = 5;
	var scr_url = "/kmeans/data?k="+classNum;
	drawChart(scr_url);
	$("#classNumber").text(classNum);
})

function drawChart(request_url){
    var COLOR_ALL = [
        '#37A2DA',
        '#e06343',
        '#37a354',
        '#b55dba',
        '#b5bd48',
        '#8378EA',
        '#96BFFF'
    ];
    myChart.showLoading();
	$.get(request_url, function(result){
	    $("#totalNumber").html(result.total);
	    $("#iteration").html(result.iteration);
        $("#sse").html(result.sse);
		var option = {
			xAxis: {min:0,max: 1},
			yAxis: {min:0,max: 1},
			series: [],
            grid:{left:150},
            color:COLOR_ALL
		};
		$.each(result.data, function(key, value){
            if(key === 'Center'){
                option.series.push({
                    name: key,
                    type: 'scatter',
                    symbolSize: 20,
                    showAllSymbol: true,
                    data: value,
                    colorBy:'data'
                });
            }else{
                option.series.push({
                    name: key,
                    type: 'scatter',
                    symbolSize: 5,
                    showAllSymbol: true,
                    data: value,
                    colorBy:'series'
                });
            }

		  });
        option.legend = {
            top:60,
            left: 0,
            orient:'vertical',
            show: true
        }
		myChart.setOption(option);
		myChart.hideLoading();
    });
}

</script>
</html>